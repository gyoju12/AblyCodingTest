package kr.mimic.ably.codingtest.common

import android.content.Context
import android.util.TypedValue
import kr.mimic.ably.codingtest.ui.BaseViewModel
import okhttp3.ResponseBody
import java.text.NumberFormat
import java.util.*

fun String?.orDefault(default: String) = this ?: default

val ResponseBody?.error: BaseViewModel.State.Error
    get() {
        return BaseViewModel.State.Error(this?.string().orDefault("오류가 발생했습니다."))
    }

fun Float.dp(context: Context): Float {
    val metrics = context.resources.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, metrics)
}

fun Int.discountRate(salePrice: Int) = ((1 - salePrice.toFloat() / this.toFloat()) * 100).toInt()

val Int.format: String
    get() {
        return NumberFormat.getNumberInstance(Locale.KOREA).format(this)
    }

val Int.purchasing: String
    get() {
        return "${this.format}개 구매중"
    }

fun <E> MutableList<E>.changeAll(elements: Collection<E>) {
    this.run {
        clear()
        addAll(elements)
    }
}

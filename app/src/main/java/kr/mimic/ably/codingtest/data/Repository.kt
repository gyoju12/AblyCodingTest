package kr.mimic.ably.codingtest.data

import android.util.Log
import kr.mimic.ably.codingtest.common.*
import kr.mimic.ably.codingtest.data.local.LikeDao
import kr.mimic.ably.codingtest.data.model.Goods
import kr.mimic.ably.codingtest.data.model.HomeRemote
import kr.mimic.ably.codingtest.data.model.Like
import kr.mimic.ably.codingtest.data.remote.TestApi
import kr.mimic.ably.codingtest.ui.BaseViewModel

class Repository(private val api: TestApi, private val db: LikeDao) {

    suspend fun getHomeData(
        success: (HomeRemote) -> Unit,
        error: (BaseViewModel.State.Error) -> Unit
    ) {
        api.getHomeData().let { response ->
            when {
                response.isSuccessful -> {
                    response.body()?.let {
                        success.invoke(it)
                    }
                }
                else -> error.invoke(response.errorBody().error)
            }
        }
    }

    suspend fun getAdditionalGoods(
        lastId: Int,
        success: (List<Goods>) -> Unit,
        error: (BaseViewModel.State.Error) -> Unit
    ) {
        api.getAdditionalGoods(lastId).let { response ->
            when {
                response.isSuccessful -> {
                    success.invoke(response.body()?.goods?.map {
                        Goods(
                            it.id,
                            it.name,
                            it.image,
                            it.actualPrice.format,
                            it.price.format,
                            it.isNew,
                            it.sellCount.purchasing,
                            it.actualPrice.discountRate(it.price),
                            hasLike(it.id)
                        )
                    }.orEmpty())
                }
                else -> error.invoke(response.errorBody().error)
            }
        }
    }

    fun getLikeList() = db.getLikeList()

    fun hasLike(id: Int) = db.hasLike(id) > 0

    fun updateLike(like: Like) {
        val hasLike = hasLike(like.id)

        when {
            hasLike -> db.deleteLike(like.id)
            else -> db.insertLike(like)
        }
    }
}
package kr.mimic.ably.codingtest.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RawQuery
import androidx.sqlite.db.SupportSQLiteQuery
import kr.mimic.ably.codingtest.data.model.Like

@Dao
interface LikeDao {

    @Query("SELECT * FROM `like` ORDER BY pid")
    fun getLikeList(): List<Like>

    @RawQuery
    fun getLikeRawQuery(query: SupportSQLiteQuery): List<Like>

    @Query("SELECT COUNT(*) FROM `like` WHERE id = :id")
    fun hasLike(id: Int): Int

    @Query("DELETE FROM `like` WHERE id = :id")
    fun deleteLike(id: Int)

    @Insert
    fun insertLike(like: Like)
}
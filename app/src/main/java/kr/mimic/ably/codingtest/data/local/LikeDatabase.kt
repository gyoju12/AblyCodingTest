package kr.mimic.ably.codingtest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import kr.mimic.ably.codingtest.data.model.Like

@Database(entities = [Like::class], version = 1)
abstract class LikeDatabase : RoomDatabase() {
    abstract fun favoriteDao(): LikeDao
}
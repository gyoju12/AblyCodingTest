package kr.mimic.ably.codingtest.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Like(
    val id: Int,
    val name: String,
    val image: String,
    val actualPrice: String,
    val price: String,
    val isNew: Boolean,
    val sellCount: String,
    val discountRate: Int
) {
    @PrimaryKey(autoGenerate = true) var pid: Int = 0
}
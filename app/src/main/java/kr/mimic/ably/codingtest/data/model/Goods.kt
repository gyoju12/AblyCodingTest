package kr.mimic.ably.codingtest.data.model

data class Goods(
    val id: Int,
    val name: String,
    val image: String,
    val actualPrice: String,
    val price: String,
    val isNew: Boolean,
    val sellCount: String,
    val discountRate: Int,
    val isLike: Boolean = false
)

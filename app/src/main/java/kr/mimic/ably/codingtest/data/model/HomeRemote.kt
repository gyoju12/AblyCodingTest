package kr.mimic.ably.codingtest.data.model

import com.google.gson.annotations.SerializedName

data class HomeRemote(
    @SerializedName("banners") val bannerRemotes: List<BannerRemote>,
    @SerializedName("goods") val goods: List<GoodsRemote>
)

data class AdditionalGoods(
    @SerializedName("goods") val goods: List<GoodsRemote>
)

data class BannerRemote(
    @SerializedName("id") val id: Int,
    @SerializedName("image") val image: String
)

data class GoodsRemote(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("image") val image: String,
    @SerializedName("actual_price") val actualPrice: Int,
    @SerializedName("price") val price: Int,
    @SerializedName("is_new") val isNew: Boolean,
    @SerializedName("sell_count") val sellCount: Int
)

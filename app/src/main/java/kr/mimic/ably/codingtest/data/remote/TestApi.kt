package kr.mimic.ably.codingtest.data.remote

import kr.mimic.ably.codingtest.data.model.AdditionalGoods
import kr.mimic.ably.codingtest.data.model.HomeRemote
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface TestApi {

    @GET("/api/home")
    suspend fun getHomeData(): Response<HomeRemote>

    @GET("/api/home/goods")
    suspend fun getAdditionalGoods(@Query("lastId") lastId: Int): Response<AdditionalGoods>
}
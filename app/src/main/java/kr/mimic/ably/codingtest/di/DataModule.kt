package kr.mimic.ably.codingtest.di

import android.util.Log
import androidx.room.Room
import kr.mimic.ably.codingtest.data.Repository
import kr.mimic.ably.codingtest.data.local.LikeDatabase
import kr.mimic.ably.codingtest.data.remote.TestApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

val roomModule = module {
    val databaseName = "like_database"

    single {
        Room.databaseBuilder(get(), LikeDatabase::class.java, databaseName)
            .setQueryCallback({ sqlQuery, bindArgs ->
                Log.d(this.javaClass.simpleName, "[SQL Query] ===> $sqlQuery :: $bindArgs")
            }, Executors.newSingleThreadExecutor())
            .build()
    }

    single {
        get<LikeDatabase>().favoriteDao()
    }
}

val networkModule = module {

    single {
        Retrofit.Builder()
            .baseUrl("http://d2bab9i9pr8lds.cloudfront.net")
            .client(OkHttpClient.Builder().apply {
                readTimeout(60, TimeUnit.SECONDS)
                connectTimeout(60, TimeUnit.SECONDS)
                writeTimeout(60, TimeUnit.SECONDS)
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            }.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TestApi::class.java)
    }
}

val repositoryModule = module {
    single {
        Repository(get(), get())
    }
}

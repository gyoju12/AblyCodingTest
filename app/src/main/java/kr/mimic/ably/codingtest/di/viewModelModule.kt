package kr.mimic.ably.codingtest.di

import kr.mimic.ably.codingtest.ui.home.ListViewModel
import kr.mimic.ably.codingtest.ui.like.LikeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ListViewModel(get()) }
    viewModel { LikeViewModel(get()) }
}
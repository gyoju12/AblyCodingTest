package kr.mimic.ably.codingtest.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    val state = MutableLiveData<State>()

    sealed class State() {
        object Loading: State()
        object Success: State()

        data class Error(val message: String = ""): State()
    }
}
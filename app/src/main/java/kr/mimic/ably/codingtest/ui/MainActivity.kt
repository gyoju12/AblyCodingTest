package kr.mimic.ably.codingtest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import kr.mimic.ably.codingtest.R
import kr.mimic.ably.codingtest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        supportActionBar?.title = resources.getString(R.string.menu_home)

        binding.bottomNavigation.setOnItemSelectedListener {
            findNavController(R.id.container).navigate(when (it.itemId) {
                R.id.menuList -> R.id.listFragment
                else -> R.id.likeFragment
            })

            supportActionBar?.title = it.title

            true
        }
    }
}
package kr.mimic.ably.codingtest.ui

import android.app.Application
import kr.mimic.ably.codingtest.di.networkModule
import kr.mimic.ably.codingtest.di.repositoryModule
import kr.mimic.ably.codingtest.di.roomModule
import kr.mimic.ably.codingtest.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

@Suppress("unused")
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            androidFileProperties()
            modules(listOf(
                networkModule,
                roomModule,
                repositoryModule,
                viewModelModule
            ))
        }
    }
}
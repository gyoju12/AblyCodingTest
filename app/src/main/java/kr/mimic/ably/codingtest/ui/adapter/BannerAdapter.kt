package kr.mimic.ably.codingtest.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kr.mimic.ably.codingtest.data.model.BannerRemote
import kr.mimic.ably.codingtest.databinding.LayoutBannerBinding
import kr.mimic.ably.codingtest.ui.viewholder.BannerViewHolder

class BannerAdapter : RecyclerView.Adapter<BannerViewHolder>() {

    var items = listOf<BannerRemote>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerViewHolder {
        val binding = LayoutBannerBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return BannerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BannerViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size
}
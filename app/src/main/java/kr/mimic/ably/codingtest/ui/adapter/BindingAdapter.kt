package kr.mimic.ably.codingtest.ui.adapter

import android.annotation.SuppressLint
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import coil.load
import coil.transform.RoundedCornersTransformation
import kr.mimic.ably.codingtest.common.dp
import kr.mimic.ably.codingtest.data.model.BannerRemote
import kr.mimic.ably.codingtest.data.model.Goods
import kr.mimic.ably.codingtest.ui.BaseViewModel
import kr.mimic.ably.codingtest.ui.home.ListViewModel
import kr.mimic.ably.codingtest.ui.like.LikeViewModel

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("bindBannerList")
    fun bindBannerList(viewPager: ViewPager2, items: ObservableList<BannerRemote>) {
        viewPager.offscreenPageLimit = 1

        if (viewPager.adapter == null) {
            viewPager.adapter = BannerAdapter()
        }

        (viewPager.adapter as BannerAdapter).items = items
        viewPager.adapter?.notifyDataSetChanged()
    }

    @JvmStatic
    @BindingAdapter("bindGoodsList")
    fun bindGoodsList(recyclerView: RecyclerView, items: ObservableList<Goods>) {
        if (recyclerView.adapter == null) {
            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 2)
            recyclerView.adapter = GoodsAdapter()
        }

        (recyclerView.adapter as GoodsAdapter).let {
            it.isLike = false
            it.items = items
        }
        recyclerView.adapter?.notifyDataSetChanged()
    }

    @JvmStatic
    @BindingAdapter("setOnCheckedLike")
    fun setOnCheckedLike(recyclerView: RecyclerView, viewModel: BaseViewModel) {
        (recyclerView.adapter as GoodsAdapter).setOnLikeChecked {
            when (viewModel) {
                is ListViewModel -> viewModel.updateLike(it)
                is LikeViewModel -> viewModel.deleteLike(it)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("bindLikeList")
    fun bindLikeList(recyclerView: RecyclerView, items: ObservableList<Goods>) {
        if (recyclerView.adapter == null) {
            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 2)
            recyclerView.adapter = GoodsAdapter()
        }

        (recyclerView.adapter as GoodsAdapter).let {
            it.isLike = true
            it.items = items
        }
        recyclerView.adapter?.notifyDataSetChanged()
    }

    @JvmStatic
    @BindingAdapter("bindImage")
    fun bindImage(imageView: AppCompatImageView, path: String) {
        imageView.load(path)
    }

    @JvmStatic
    @BindingAdapter("bindRoundImage")
    fun bindRoundImage(imageView: AppCompatImageView, path: String) {
        imageView.apply {
            scaleType = ImageView.ScaleType.CENTER_CROP
        }.load(path) {
            transformations(RoundedCornersTransformation(4f.dp(imageView.context)))
        }
    }

    @JvmStatic
    @BindingAdapter("bindIsLike")
    fun bindIsLike(imageView: AppCompatImageView, checked: Boolean = false) {
        imageView.isSelected = checked
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("bindDiscountRate")
    fun bindDiscountRate(textView: AppCompatTextView, rate: Int) {
        textView.run {
            text = "$rate%"
            isVisible = rate != 0
        }
    }
}
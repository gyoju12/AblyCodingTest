package kr.mimic.ably.codingtest.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kr.mimic.ably.codingtest.data.model.Goods
import kr.mimic.ably.codingtest.databinding.LayoutGoodsBinding
import kr.mimic.ably.codingtest.ui.viewholder.GoodsViewHolder

class GoodsAdapter : RecyclerView.Adapter<GoodsViewHolder>() {
    
    var items = listOf<Goods>()
    var isLike = false

    private var onCheck: ((Goods) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoodsViewHolder {
        val binding = LayoutGoodsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        
        return GoodsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GoodsViewHolder, position: Int) {
        holder.bind(items[position], isLike, onCheck)
    }

    override fun getItemCount() = items.size

    fun setOnLikeChecked(check: ((Goods) -> Unit)?) {
        onCheck = check
    }
}
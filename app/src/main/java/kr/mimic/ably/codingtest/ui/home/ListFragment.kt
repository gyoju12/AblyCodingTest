package kr.mimic.ably.codingtest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import kotlinx.coroutines.launch
import kr.mimic.ably.codingtest.R
import kr.mimic.ably.codingtest.databinding.ListFragmentBinding
import kr.mimic.ably.codingtest.ui.BaseViewModel.State
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment : Fragment() {

    private lateinit var binding: ListFragmentBinding

    private val viewModel by viewModel<ListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false)

        binding.refreshLayout.setOnRefreshListener {
            viewModel.initHome()
        }

        binding.bannerPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.setBannerPosition(position)
            }
        })

        binding.goodsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                (recyclerView.layoutManager as GridLayoutManager).let { gridLayoutManager ->
                    gridLayoutManager.findLastCompletelyVisibleItemPosition().takeIf {
                        it + 1 >= gridLayoutManager.itemCount
                    }?.let {
                        lifecycleScope.launch {
                            viewModel.getAdditionalGoods()
                        }
                    }
                }
            }
        })

        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner) {
            when (it) {
                is State.Error -> {
                    binding.refreshLayout.isRefreshing = false
                    Toast.makeText(view.context, it.message, Toast.LENGTH_SHORT).show()
                }
                is State.Loading,
                is State.Success -> {
                    binding.refreshLayout.isRefreshing = it is State.Loading
                }
            }
        }

        viewModel.initHome()
    }
}
package kr.mimic.ably.codingtest.ui.home

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kr.mimic.ably.codingtest.common.changeAll
import kr.mimic.ably.codingtest.common.discountRate
import kr.mimic.ably.codingtest.common.format
import kr.mimic.ably.codingtest.common.purchasing
import kr.mimic.ably.codingtest.data.Repository
import kr.mimic.ably.codingtest.data.model.BannerRemote
import kr.mimic.ably.codingtest.data.model.Goods
import kr.mimic.ably.codingtest.data.model.Like
import kr.mimic.ably.codingtest.ui.BaseViewModel

class ListViewModel(private val repository: Repository) : BaseViewModel() {

    val banners = ObservableArrayList<BannerRemote>()
    val goodsList = ObservableArrayList<Goods>()

    val bannerCount = ObservableField<String>()

    fun initHome() {
        viewModelScope.launch(Dispatchers.IO) {
            state.postValue(State.Loading)

            repository.getHomeData({ home ->
                banners.changeAll(home.bannerRemotes)
                goodsList.changeAll(home.goods.map {
                    Goods(
                        it.id,
                        it.name,
                        it.image,
                        it.actualPrice.format,
                        it.price.format,
                        it.isNew,
                        it.sellCount.purchasing,
                        it.actualPrice.discountRate(it.price),
                        repository.hasLike(it.id)
                    )
                })

                state.postValue(State.Success)
            }, {
                state.postValue(it)
            })
        }
    }

    fun getAdditionalGoods() {
        viewModelScope.launch(Dispatchers.IO) {
            state.postValue(State.Loading)

            goodsList.takeIf { it.isNotEmpty() }?.let { list ->
                repository.getAdditionalGoods(list.last().id, {
                    goodsList.addAll(it)
                    state.postValue(State.Success)
                }, {
                    state.postValue(it)
                })
            }
        }
    }

    fun setBannerPosition(position: Int) {
        viewModelScope.launch {
            bannerCount.set("${position + 1}/${banners.size}")
        }
    }

    fun updateLike(goods: Goods) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateLike(Like(
            goods.id,
            goods.name,
            goods.image,
            goods.actualPrice,
            goods.price,
            goods.isNew,
            goods.sellCount,
            goods.discountRate
        ))

        goodsList.filter {
            it.id == goods.id
        }.forEach {
            val index = goodsList.indexOf(it)
            goodsList[index] = Goods(
                goods.id,
                goods.name,
                goods.image,
                goods.actualPrice,
                goods.price,
                goods.isNew,
                goods.sellCount,
                goods.discountRate,
                goods.isLike.not()
            )
        }
    }
}
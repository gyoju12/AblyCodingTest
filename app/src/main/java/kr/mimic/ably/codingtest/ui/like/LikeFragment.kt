package kr.mimic.ably.codingtest.ui.like

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import kr.mimic.ably.codingtest.R
import kr.mimic.ably.codingtest.databinding.LikeFragmentBinding
import kr.mimic.ably.codingtest.ui.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LikeFragment : Fragment() {

    private lateinit var binding: LikeFragmentBinding

    private val viewModel by viewModel<LikeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.like_fragment, container, false)

        binding.refreshLayout.setOnRefreshListener {
            viewModel.initLike()
        }

        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner) {
            when (it) {
                is BaseViewModel.State.Error -> {
                    binding.refreshLayout.isRefreshing = false
                    Toast.makeText(view.context, it.message, Toast.LENGTH_SHORT).show()
                }
                is BaseViewModel.State.Loading,
                is BaseViewModel.State.Success -> {
                    binding.refreshLayout.isRefreshing = it is BaseViewModel.State.Loading
                }
            }
        }

        viewModel.initLike()
    }
}
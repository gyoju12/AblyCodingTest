package kr.mimic.ably.codingtest.ui.like

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kr.mimic.ably.codingtest.common.changeAll
import kr.mimic.ably.codingtest.data.Repository
import kr.mimic.ably.codingtest.data.model.Goods
import kr.mimic.ably.codingtest.data.model.Like
import kr.mimic.ably.codingtest.ui.BaseViewModel

class LikeViewModel(private val repository: Repository) : BaseViewModel() {

    val likes = ObservableArrayList<Goods>()

    fun initLike() {
        viewModelScope.launch(Dispatchers.IO) {
            state.postValue(State.Loading)

            likes.changeAll(repository.getLikeList().map {
                Goods(
                    it.id,
                    it.name,
                    it.image,
                    it.actualPrice,
                    it.price,
                    it.isNew,
                    it.sellCount,
                    it.discountRate,
                    true
                )
            })

            state.postValue(State.Success)
        }
    }

    fun deleteLike(goods: Goods) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateLike(Like(
                goods.id,
                goods.name,
                goods.image,
                goods.actualPrice,
                goods.price,
                goods.isNew,
                goods.sellCount,
                goods.discountRate
            ))

            likes.remove(goods)
        }
    }
}
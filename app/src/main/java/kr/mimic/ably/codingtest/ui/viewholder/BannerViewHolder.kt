package kr.mimic.ably.codingtest.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import kr.mimic.ably.codingtest.data.model.BannerRemote
import kr.mimic.ably.codingtest.databinding.LayoutBannerBinding

class BannerViewHolder(private val viewBinding: LayoutBannerBinding) : RecyclerView.ViewHolder(viewBinding.root) {

    fun bind(banner: BannerRemote) {
        viewBinding.banner = banner
    }
}
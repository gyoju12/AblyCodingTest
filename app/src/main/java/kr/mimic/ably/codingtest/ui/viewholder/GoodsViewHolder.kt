package kr.mimic.ably.codingtest.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import kr.mimic.ably.codingtest.data.model.Goods
import kr.mimic.ably.codingtest.databinding.LayoutGoodsBinding

class GoodsViewHolder(private val viewBinding: LayoutGoodsBinding) : RecyclerView.ViewHolder(viewBinding.root) {

    fun bind(goods: Goods, isLike: Boolean, onCheck: ((Goods) -> Unit)?) {
        viewBinding.goods = goods
        viewBinding.isLike = isLike
        viewBinding.root.setOnClickListener {
            onCheck?.invoke(goods)
        }
    }
}